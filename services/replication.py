from postprocessor.database_record import get_value_from_record


def migrate(source, destination):
    latest_id = get_last_changed_id(destination)
    latest_changes = get_latest_changes(source, latest_id)

    has_changed_happened = False
    if latest_changes:
        has_changed_happened = True

    for change in latest_changes:
        migrate_row(destination, change)

    return has_changed_happened


def migrate_row(destination, change):
    id = change[0]
    action = change[5]
    old_data = change[6]
    new_data = change[7]

    print(f"Change ID: {id}", flush=True)

    if action == "U":
        print("-------------------------------", flush=True)
        print("New update happened", flush=True)
        print(f"{old_data} -> {new_data}", flush=True)
        print("-------------------------------", flush=True)
        update(destination, old_data, new_data)
    elif action == "I":
        print("-------------------------------", flush=True)
        print("New insert happened", flush=True)
        print(f"Insert: {new_data}", flush=True)
        print("-------------------------------", flush=True)
        insert(destination, new_data)
    elif action == "D":
        print("-------------------------------", flush=True)
        print("New deletion happened", flush=True)
        print(f"Delete: {old_data}", flush=True)
        print("-------------------------------", flush=True)
        delete(destination, old_data)

    return id


def delete(destination, old_data):
    id = int(get_value_from_record(old_data, 0))
    cursor = destination.cursor()
    cursor.execute(f"DELETE FROM condition WHERE id={id};")
    cursor.close()


def insert(destination, new_data):
    camera_id = get_value_from_record(new_data, 1)
    threshold = int(get_value_from_record(new_data, 2))
    operator = get_value_from_record(new_data, 3)
    actions = get_value_from_record(new_data, 4)
    occurrence = get_value_from_record(new_data, 6)
    name = get_value_from_record(new_data, 7)

    cursor = destination.cursor()
    cursor.execute(
        f"INSERT INTO condition "
        f"(camera_id, threshold, operator, actions, occurrence, last_occurred, name)"
        f"VALUES ('{camera_id}', {threshold}, '{operator}', '{actions}', '{occurrence}', Null, '{name}');")
    cursor.close()


def update(destination, old_data, new_data):
    new_id = int(get_value_from_record(new_data, 0))
    old_id = int(get_value_from_record(old_data, 0))
    camera_id = get_value_from_record(new_data, 1)
    threshold = int(get_value_from_record(new_data, 2))
    operator = get_value_from_record(new_data, 3)
    actions = get_value_from_record(new_data, 4)
    last_occurred = get_value_from_record(new_data, 5)
    occurrence = get_value_from_record(new_data, 6)
    name = get_value_from_record(new_data, 7)

    cursor = destination.cursor()

    if is_last_occurred_time_updated(old_data, new_data):
        cursor.execute(f"UPDATE condition SET id={new_id}, last_occurred='{last_occurred}' WHERE id={old_id};")

    else:
        cursor.execute(f"UPDATE condition SET id={new_id}, camera_id='{camera_id}', name='{name}', threshold={threshold},"
                       f"operator='{operator}', actions='{actions}', occurrence='{occurrence}' WHERE id={old_id};")
    cursor.close()


def is_last_occurred_time_updated(old_data, new_data):
    old_last_occurred = get_value_from_record(old_data, 6)
    new_last_occurred = get_value_from_record(new_data, 6)

    if old_last_occurred != new_last_occurred:
        return True
    else:
        return False


def get_latest_changes(connection_source, last_id):
    cursor = connection_source.cursor()
    cursor.execute(f"SELECT * FROM audit.logged_actions WHERE id>{last_id};")
    changes = cursor.fetchall()
    cursor.close()
    return changes


def get_last_changed_id(connection_source):
    cursor = connection_source.cursor()
    cursor.execute("SELECT MAX(id) FROM audit.logged_actions;")
    id = cursor.fetchone()
    cursor.close()
    if id[0]:
        return id[0]
    return 0
