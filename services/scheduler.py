import os
from config.db_connection import initiate_connection
from services.replication import migrate

master_host = os.environ["POSTGRES_HOST_MASTER"]
slave_host = os.environ["POSTGRES_HOST_SLAVE"]


class Scheduler:
    master_db_connection = None
    slave_db_connection = None

    def schedule_migration(self):
        try:
            has_change_happened = migrate(self.slave_db_connection, self.master_db_connection)
            if has_change_happened:
                print("-------------------------------", flush=True)
                print("Sync slave into master", flush=True)
                print("-------------------------------", flush=True)

            has_change_happened = migrate(self.master_db_connection, self.slave_db_connection)
            if has_change_happened:
                print("-------------------------------", flush=True)
                print("Sync master into slave", flush=True)
                print("-------------------------------", flush=True)
        except Exception:
            self.connect_to_db()

    def connect_to_db(self):
        try:
            if not self.master_db_connection or self.master_db_connection.closed:
                self.master_db_connection = initiate_connection(master_host)
            if not self.slave_db_connection or self.slave_db_connection.closed:
                self.slave_db_connection = initiate_connection(slave_host)
        except Exception:
            print("-------------------------------", flush=True)
            print("Could not connect to database", flush=True)
            print("-------------------------------", flush=True)


