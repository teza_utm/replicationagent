import os
import time
from services.scheduler import Scheduler


replication_interval = float(os.environ["INTERVAL"])

if __name__ == "__main__":
    scheduler = Scheduler()
    scheduler.connect_to_db()
    print("-------------------------------", flush=True)
    print("Replication worker have started", flush=True)
    print("-------------------------------", flush=True)

    while True:
        scheduler.schedule_migration()
        time.sleep(replication_interval)
