import os
import psycopg2
from psycopg2 import Error

user = os.environ["POSTGRES_USER"]
password = os.environ["POSTGRES_PASSWORD"]
port = os.environ["POSTGRES_PORT"]
database = os.environ["POSTGRES_DB"]


def initiate_connection(host):
    try:
        connection = psycopg2.connect(user=user,
                                      password=password,
                                      host=host,
                                      port=port,
                                      database=database)

        connection.set_session(autocommit=True)
        return connection
    except (Exception, Error) as error:
        print("Error while connecting to PostgreSQL", error)
