FROM python:3.7-slim

COPY requirements.txt .

RUN apt-get update && apt-get install netcat-openbsd -y

RUN pip3 install -r requirements.txt

COPY . .

RUN chmod +x wait-for-it.sh