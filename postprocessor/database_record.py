def get_brackets_index(values, searched_element):
    return [i for i, s in enumerate(values) if searched_element in s]


def unify_objects(columns):
    result = []
    column_index = 0
    while column_index < len(columns):
        column = columns[column_index]
        if column.startswith(('"[', '"{')):
            sublist = columns[column_index:]
            brackets_index = get_brackets_index(sublist, ']"')
            if not brackets_index:
                brackets_index = get_brackets_index(sublist, '}"')
            unified_object = ','.join(sublist[0:brackets_index[0] + 1])
            json_object = convert_string_to_json(unified_object)
            result.append(json_object)
            column_index = brackets_index[0] + column_index + 1
        else:
            result.append(column)
            column_index += 1
    return result


def get_value_from_record(row, column):
    row = row.replace("(", "").replace(")", "")
    columns = row.split(",")
    columns = unify_objects(columns)
    return columns[column]


def convert_string_to_json(string):
    value = string.replace('""', '"')
    return value[1:-1]
